<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

return [
    // 日志记录方式，内置 file 支持扩展
    'type'        => 'File',
    // 日志保存目录
    'path'        => ROOT.'/var/log',
    // 日志记录级别
    'level'       => [],
    // 单文件日志写入
    'single'      => false,
    // 独立日志级别
    'apart_level' => [],
    // 最大日志文件数量
    'max_files'   => 0,
    // 是否关闭日志写入
    'close'       => false,
    // 更多选项
    'option'      => [
        // 'path' => ROOT.'/var/log/',
    ],
];
