<?php
namespace app\controller;

use dxing\Controller;
use app\model\GuestbookModel;

class GuestbookController extends Controller{

    protected $guestbookModel;

    public function __construct(){
        $this->guestbookModel = new GuestbookModel;
    }

    // 所有留言
    public function index(){
        $data = $this->guestbookModel->all();
        dump($data);
    }

    // 添加留言
    public function add(){

    }

    // 保存留言
    public function save(){

    }
}
