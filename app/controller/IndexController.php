<?php
namespace app\controller;

# 如果使用的类命名空间和现命名空间一致，可不用再次use
use dxing\Controller;
use dxing\Model;
use dxing\Config;
use dxing\Log;
use app\model\LangModel;

class IndexController extends Controller
{
    public function index()
    {
        echo 123;die;
    }

    public function index1()
    {
        // 测试Model模型
        $model = new Model;
        // dump($model);
        // $sql = 'SELECT * FROM lang';
        // $res = $model->query($sql);
        // var_dump($res->fetchAll());
        // 测试继承medoo数据库框架
        $langModel = new LangModel;
        // $res = $langModel->lists();
        // $res = $langModel->getOne(1);
        // $res = $langModel->setOne(2,[
        //     'name' => 'JS',
        //     'sort' => '2'
        // ]);
        // $res = $langModel->delOne(2);
        // $res = $langModel->create([
        //     'name' => 'python',
        //     'sort' => 8
        // ]);
        // dump($res);
        // $langModel->index();

        // 测试配置项
        // echo Config::get('default_controller','app');
        // echo Config::get('default_action','app');
        // var_dump(Config::$config);

        // 测试日志类
        // $log = Log::init();

        // 测试错误提示是否屏蔽
        // sss();

        // 测试 普通模板 & Twig模板引擎
        $data = 'Hello World';
        // $title = '视图文件';
        //
        // $this->assign('title',$title);
        $this->assign('data',$data);
        $this->display('index.html');
    }

    public function test()
    {
        # 模板布局测试
        $data = 'Hello World';
        $this->assign('data',$data);
        $this->display(); // 空值 index/test.html
    }
}
