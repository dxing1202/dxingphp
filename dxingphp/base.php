<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

# 入口引入文件
# 1.定义常量
# 2.引入composer自动加载类
# 3.加载自动加载类
# 4.启动框架
# 获取当前根目录

define( 'ROOT',dirname(__DIR__) );
# 框架核心目录
define('DXINGPHP',ROOT.'/dxingphp');
# APP目录
define('APP',ROOT.'/app');
# APP的命名空间
define('MODULE','/app');

define('DEBUG',true);

# 引入composer自动加载类
include "../vendor/autoload.php";

if(DEBUG){
    # 使用whoops调试第三方类
    // $whoops = new \Whoops\Run;
    # Whoops! There was an error.
    // $errorTitle = '哎呀！ 有一个错误'; // 定义一个标题
    // $whoopsClass = new \Whoops\Handler\PrettyPageHandler;
    // $whoopsClass->setPageTitle($errorTitle); // 修改标题
    // $whoops->pushHandler($whoopsClass);
    // $whoops->register();

    ini_set('display_errors','On');
}else{
    // ini_set('display_errors','Off'); // 直接修改PHP配置关闭错误提示
    error_reporting(0); // 屏蔽所有错误提示
}

# 载入Loader类
require DXINGPHP . '/library/dxing/Loader.php';
# 注册自动加载
Loader::register();

# 引导加载启动框架
Boot::run();
