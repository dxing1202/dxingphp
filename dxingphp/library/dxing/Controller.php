<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

/**
 * 控制器基础类
 */
class Controller{

    /**
     * 传变量的数组
     * @var [type]
     */
    public $data;

    /**
     * 当前运行控制器
     * @var string
     */
    public $current_controller;

    /**
     * 当前运行方法
     * @var string
     */
    public $current_action;

    public function __construct(){
        # 定义$current_controller 当前控制器
        $this->current_controller = (new Route)->controller;
        # 定义$current_action 当前方法
        $this->current_action = (new Route)->action;
    }

    /**
     * 模板变量赋值
     * @access public
     * @param  mixed $name
     * @param  mixed $value
     * @return void
     */
    public function assign($name,$value = ''){

        if (is_array($name)) {
            $this->data = array_merge($this->data, $name);
        } else {
            $this->data[$name] = $value;
        }
    }

    /**
     * 引入视图文件
     * @param  string $file = null 文件名称
     * @return [type]       [description]
     */
    public function display($file = null){
        # 如果传入的值$file是空 则使用当前的控制器和方法 组成index/index.html
        $file = $file ?? $this->current_controller.'/'.$this->current_action.'.html';
        # 组成模板绝对路径
        $filePath = str_replace('\\','/',APP.'/view/'.$file);
        # 判断模板文件是否存在
        if(is_file($filePath)){
            # 下面两条代码做的是普通的变量和模板
            # 将数组中的键值做变量名，数组中的值作为变量的值。一个个弄出来
            // extract($this->assign);
            # 引入文件 普通文件
            // include $file;

            # Twig模板引擎
            # 模板存在的路径
            $tempPath = str_replace('\\','/',APP.'/view');
            $tempCachePath = str_replace('\\','/',ROOT.'/var/cache');
            // dump($tempCachePath);exit;
            # 配置Twig 加载模板
            $loader = new \Twig_Loader_Filesystem($tempPath);
            $twig = new \Twig_Environment($loader, [
                # 缓存存放目录
                'cache' => $tempCachePath,
                // 'debug' => true,
            ]);
            # 一举加载和渲染模板 模板，变量数组
            $twig->display($file,$this->data ?? []);
            # 加载模板
            // $template = $twig->load('index.html');
            # 调用render()方法 使用某些变量呈现模板 ??判断是否存在，存在则用前面的否则后面的 PHP7.0以上
            # 使用render方法 需要echo
            // echo $template->render($this->assign ?? '');
            # 该display()方法是直接输出模板的快捷方式
            // $template->display($this->assign ?? '');

        }
    }
}
