<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

use Medoo\Medoo;

/**
 * 模型类 继承 Medoo第三方轻量级的PHP数据库框架 也可以自定义使用PDO
 */
class Model extends Medoo{

    /**
     * 定义自增ID
     * @var string
     */
    public $uuid = 'id';

    /**
     * 定义表名
     * @var string
     */
    public $table = '';

    public function __construct(){
        # 获取database配置文件里面的配置数据
        $option = Config::oneAll('database');

        parent::__construct($option);

    }

    /**
     * 新增数据
     * @param  array $data 数据数组
     * @return int 返回添加数据成功后的ID
     */
    public function create($data){
        $this->insert($this->table,$data);
        // 获取最后插入行的ID
        return $this->id();
    }

    /**
     * 获取所有数据
     * @return array 返回所有数据
     */
    public function all(){
        return $this->select($this->table,'*');
    }

    /**
     * 根据ID获取到单条数据
     * @param  int|string $id id
     * @return array    返回单条数据
     */
    public function getOne($id){
        return $this->get($this->table,'*',[
            $this->uuid => $id,
        ]);
    }

    /**
     * 根据ID修改单条数据
     * @param int|string $id   id
     * @param array $data 修改的数据
     * @return int 返回受影响的行数
     */
    public function setOne($id,$data){
        $res = $this->update($this->table,$data,[
            $this->uuid => $id
        ]);
        // 返回受上一个SQL语句影响的行数
        return $res->rowCount();
    }

    /**
     * 根据ID删除单条数据
     * @param  int|string $id   id
     * @return int 返回受影响的行数
     */
    public function delOne($id){
        $res = $this->delete($this->table,[
            $this->uuid => $id
        ]);
        return $res->rowCount();
    }
}

// # 用回PDO的时候使用
// # $dsn,$username,$passwd,$options
// public function __construct(){
//     # 获取database配置文件里面的配置数据
//     $database = Config::oneAll('database');
//
//     # 组成dsn字符串
//     $dsn = $database['type'].':host='.$database['hostname'].';dbname='.$database['database'];
//     # 用户名
//     $username = $database['username'];
//     # 密码
//     $passwd = $database['password'];
//     try{
//         parent::__construct($dsn,$username,$passwd);
//     }catch(PDOException $e){
//         echo $e->getMessage();
//     }
// }
