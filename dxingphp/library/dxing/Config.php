<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

/**
 * 配置类
 */
class Config{

    static public $config = [];

    /**
     * 解析配置文件属性到变量$config = []
     * @var string $fileName 文件名
     */
    static public function parse($fileName){
        self::$config[$fileName] = self::loadFile($fileName);
    }

    /**
     * 加载配置文件 默认文件名：app,database,route
     * @var array $fielNameArray 文件名数组
     */
    static public function load($fileNameArray = ['app','database','route']){
        foreach($fileNameArray as $v){
            self::parse($v);
        }
    }

    /**
     * 加载配置文件
     * @var string $fileName 文件名
     * @var string $suffix 文件类型 默认php
     * @return array 配置文件数组
     */
    static public function loadFile($fileName,$suffix = 'php'){
        # 组成文件绝对路径
        $filePath = ROOT.'/config/'.$fileName.'.'.$suffix;
        # 判断文件是否存在
        if(is_file($filePath)){
            # 引入文件直接返回文件里面的数组
            return include $filePath;
        }else{
            throw new \Exception('找不到配置文件');
        }
    }


    static public function get($name){
        /**
         * 1.判断配置文件是否存在
         * 2.判断配置是否存在
         * 3.缓存配置
         */

        # 输入格式 app.default_controller 前面的是文件名 后面的是属性名
        # 现在最多是两级，待后期再次开发
        $nameArr = explode('.',$name);
        $count = count($nameArr);
        if($count === 2){
            $fileName = $nameArr[0];
            $name = $nameArr[1];
        }else{
            throw new \Exception('你输入有误，请认真看文档！');
        }

        if(isset(self::$config[$fileName])){
            return self::$config[$fileName][$name];
        }else{
            # 加载文件
            $conf = self::loadFile($fileName);
            # 判断配置项是否存在
            if(isset($conf[$name])){
                self::$config[$fileName] = $conf;
                return $conf[$name];
            }else{
                throw new \Exception('没有这个配置项'.$name);
            }



        }
    }

    /**
     * 获取单配置文件全部配置属性
     * @var string $fileName 文件名
     * @return array $conf 配置数组
     */
    static public function oneAll($fileName){

        if(isset(self::$config[$fileName])){
            return self::$config[$fileName];
        }else{
            # 如果找不到文件，会在loadFile方法中直接跳出异常
            $conf = self::loadFile($fileName);
            self::$config[$fileName] = $conf;
            return $conf;
        }
    }

    /**
     * 获取所有已加载好的配置数据
     * @var [type]
     * @return array 配置数据
     */
    static public function all(){
        return self::$config;
    }

}
