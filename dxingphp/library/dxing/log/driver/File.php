<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing\log\driver;

use dxing\Config;

/**
 * 文件日志系统
 */
class File{

    /**
     * 日志存储位置
     * @var [type]
     */
    public $path;

    public function __construct(){
        $this->path = Config::get('log.path');
    }

    /**
     * 1.确定文件存储位置是否存在 否则新建目录
     * 2.写入日志
     * @param  string $message 日志信息
     * @param  string $file 日志文件
     * @param  string $suffix 日志文件后缀名
     * @return [type]       [description]
     */
    public function log($message,$file = 'log',$ext = 'txt'){
        # 判断路径目录是否存在否则新建
        if(!is_dir($this->path)){
            # 权限777，递归创建
            mkdir($this->path,'0777',true);
        }
        # 检测当前年月日目录 不存在则新建一个
        $dateDir = $this->path.'/'.date('Ymd');
        if(!is_dir($dateDir)){
            mkdir($dateDir,'0777',true);
        }
        # 日志文件绝对路径
        $filePath = $dateDir.'/'.$file.'.'.$ext;
        // var_dump($filePath);exit;
        # 对信息数据进行Json编码 + 换行
        $message = date('Y-m-d H:i:s') . ' ' . json_encode($message).PHP_EOL;
        # 写入文件，如果没有文件则自动新建一个文件 追加添加数据，不覆盖原来的
        return file_put_contents($filePath,$message,FILE_APPEND);



    }
}
