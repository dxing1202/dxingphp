<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

class Boot{

    /**
     * 构造方法
     */
    public function __construct(){
        # 加载路由类
        // self::$routeBoot = new Route;
        # 加载控制器类
        // self::$controllerBoot = new Controller(self::$routeBoot->controller,self::$routeBoot->action);
        # 加载模型类
        // new Model;

    }

    static public function run(){
        # new自身 ，触发构造方法
        // new self;

        # 加载路由类
        $routeBoot = new Route;
        # 加载配置类
        Config::load(['app','database','route','log']);
        # 加载日志类
        Log::init();
        # 加载模型类 没必要new模型类，要用的时候直接创建model继承使用
        // new Model;
        # 加载链接的控制器，方法
        self::callCtrl($routeBoot->controller,$routeBoot->action);

    }

    /**
     * 调用控制器
     * 链接的控制，方法，new命名空间
     * @var [type]
     */
    static public function callCtrl($controller,$action){
        # 控制器文件绝对路径
        $controllerFile = APP.'/controller/'.$controller.'Controller'.'.php';

        if( is_file($controllerFile) ){
            # 引入文件
            include $controllerFile;
            # 设置命名空间
            $namespace = str_replace('/','\\',MODULE.'/controller/'.ucfirst($controller).'Controller');
            # new namespace
            $ctrl = new $namespace;
            # 使用控制器对应的方法
            $ctrl->$action();

            # 添加日志
            $logMessage = 'controller:'.$controller.' '.'action:'.$action;
            Log::log($logMessage);
        }else{
            throw new \Exception('找不到控制器'.$controller);
        }
    }


}
