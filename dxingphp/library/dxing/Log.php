<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

class Log{

    static public $class;

    /**
     * 1.确定日志的存储方式
     * 2.写日志
     */

    static public function init(){
        // 确定存储方式
        # 根据在配置文件中取到存储方式
        $driver = Config::get('log.type');
        # 命名空间 + 驱动类型
        $class = '\dxing\log\driver\\'.$driver;
        # new 实例化 驱动类文件
        self::$class = new $class;
    }

    static public function log($name,$file = 'log'){
        self::$class->log($name,$file);
    }
}
