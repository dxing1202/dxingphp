<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

class Loader{

    static public $classMap = [];

    /**
     * 注册自动加载机制
     */
    static public function register($autoload = ''){
        # 调用函数，类，不存在则使用\dxing\Loader::autoload函数进行查找
        spl_autoload_register('\dxing\Loader::autoload');
    }

    /**
     * 自动加载
     */
    static public function autoload($class){
        # 判断是否已经引入加载，已加载则返回true
        if( isset($classMap[$class]) ){
            return true;
        }else{
            # 根据传入$class的分配文件路径
            $file = self::filePath($class);

            if(is_file($file)){
                include $file;
                self::$classMap[$class] = $class;
            }else{
                return false;
            }
        }
    }

    static public function filePath($class){
        # 根据 \ 符号进行分割成数组
        $classArr = explode('\\',$class);
        # 如果第一个是app 则代表是app目录下 初衷是为了能调用到model
        if($classArr[0] === 'app'){
            return str_replace('\\','/',ROOT.'/'.$class.'.php');
        }else{
            # 其他都是默认从本文件的目录进行查找，调用自身核心的函数库
            return str_replace('\\','/',dirname(__DIR__).'/'.$class.'.php');
        }
    }



}
