<?php
/**
 * DXingPHP
 * Copyright (c) 2019 http://dxingphp.cn All rights reserved
 * @author DXing1202 <dxing1202.cn>
 * @version 1.0 dev
 */

namespace dxing;

use dxing\Config;

class Route{

    /**
     * xxx.com/index/index
     * xxx.com/index.php/index/index
     * 1.隐藏index.php
     * 2.获取URL 参数部分
     * 3.返回对应控制器和方法
     */

    public $controller;

    public $action;

    public function __construct(){

        if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/'){
            // /index/index
            $path = $_SERVER['REQUEST_URI'];
            $patharr = explode( '/',trim($path,'/') );
            if(isset($patharr[0])){
                $this->controller = $patharr[0];
                unset($patharr[0]);
            }
            if(isset($patharr[1])){
                $this->action = $patharr[1];
                unset($patharr[1]);
            }else{
                $this->action = Config::get('app.default_action');
            }

            // url多余部分转换成 GET
            // id/1/str/2/test/3
            $count = count($patharr) + 2;
            $i = 2;
            while($i < $count){
                if(isset($patharr[$i+1])){
                    $_GET[$patharr[$i]] = $patharr[$i+1];
                }
                $i = $i+ 2;
            }

        }else{
            $this->controller = Config::get('app.default_controller');
            $this->action = Config::get('app.default_action');
        }
    }

}
