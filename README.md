[DXingPHP官网](http://www.dxingphp.cn)

DXingPHP 1.0 —— 轻便的PHP框架

===============

> DXingPHP1.0的运行环境要求PHP7.0+。

## 缺陷

+ 暂时还没有模块

## 参考其他前辈框架
+ ThinkPHP 5.1
+ PPPHP
+ Symfony

## 在线手册

+ [完全开发手册](http://www.dxingphp.cn/manual/dxingphp/content)

## 命名规范

`DXingPHP1.0`遵循PSR-2命名规范和PSR-4自动加载规范。

## 版权信息

DXingPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2019-2020 by DXingPHP (http://dxingphp.cn)
All rights reserved。


DXingPHP® 商标和著作权所有者为 dXing1202.cn。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
