[DXing1202 Blog](http://dxing1202.cn)

DXing1202 自传 - 一心只想撞南墙的小星星

===============

 + 自己学习的所有精华都会集中在此框架中
 + 也会将自己觉得一些好用的插件或者类库都是用在controller实例
 + 敬请期待

### **框架初始所用的插件，类库**
 + 数据库框架：Medoo 一个轻量级的PHP数据库框架
  - Medoo框架也是使用PDO原类改过来的
  - 或者直接使用PHP自带PDO类
  - 不要再使用mysql了，要用也最少是mysqli。推荐：PDO
 + 模板引擎：Twig symfony框架使用的模板引擎
  - 类似还有： smarty引擎

### **第三方类库 composer**
 + filp/whoops 一个美美的异常显示页面
 + symfony/var-dumper 一个dump 美化显示的
 + catfan/medoo 一个轻量级的PHP数据库框架
 + twig/twig symfony框架使用的模板引擎
