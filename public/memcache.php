<?php

$m = new Memcached();

// $m->addServer() host, port 向连接池中添加一个memcache服务器
$m->addServer('127.0.0.1', 11211);
$m->addServer('127.0.0.2', 11211);

// $m->add() key, value, flag(压缩), expire(有效时间) 增加一个条目到缓存服务器
$m->add('mkey', 'mvalue', false, 600);
// 同个key使用add是不能替换的，需要使用replace
// $m->replace() key, value, flag(压缩), expire(有效时间) 替换已经存在的元素的值
$m->replace('mkey', 'mvalue2', false, 600);

echo $m->get('mkey');

// $m->getStats() 获取服务器统计信息
// print_r($m->getStats());
// $m->getVersion() 返回服务器版本信息
// print_r($m->getVersion());
// var_dump($m);
